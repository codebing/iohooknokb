## About
Node.js global native mouse listener. not  keyboard 
This module can  mouse events via native hooks inside and outside your JavaScript/TypeScript application.

## Platform support

- Versions >= 0.6.0 support only officially supported platforms versions.
- Versions 0.5.X are the last to support Electron < 4.0.0
- Versions 0.4.X are the last to support for Node < 8.0 and Electron < 2.0.0

## Installation

iohooknokb provides prebuilt version for a bunch of OSes and platforms.

### Linux (including WSL)

```bash
# On Linux (including WSL) platform, you will need libxkbcommon-x11 installed
sudo apt-get install -y libxkbcommon-x11-0
```

### All platforms

```bash
npm install iohooknokb --save # or yarn add iohooknokb
```

## FAQ
Q. 
```
error:: Error: The module '<project>/node_modules/electron/node_modules/ref/build/Release/binding.node'
was compiled against a different Node.js version using
NODE_MODULE_VERSION 57. This version of Node.js requires
NODE_MODULE_VERSION 54. Please try re-compiling or re-installing
the module (for instance, using `npm rebuild` or`npm install`)
```
A. 
```bash
npm i electron-rebuild -s
./node_modules/.bin/electron-rebuild
```

## Contributors

Thanks [iohook](https://github.com/wilix-team/iohook) project 
