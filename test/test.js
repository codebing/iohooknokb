const ioHook = require('..');
ioHook.on('mousedown', (event) => {
  console.log('mousedown', event);
  /* You get object like this
      {
        type: 'mousemove',
        x: 700,
        y: 400
      }
     */
});
ioHook.on('mouseup', (event) => {
  console.log('mouseup', event);
  /* You get object like this
        {
          type: 'mousemove',
          x: 700,
          y: 400
        }
        */
});

ioHook.on('keydown', (event) => {
  console.log('keydown', event);
  /* You get object like this
          {
            type: 'mousemove',
            x: 700,
            y: 400
          }
          */
});

//Register and start hook
ioHook.start();
